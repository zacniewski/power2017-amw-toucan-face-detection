# README #

System przetwarzania obrazów w przeglądarce

### Cel projektu ###

* Realizacja detekcji twarzy w przeglądarce internetowej

### Technologie ###

* tracking.js, HTML

### Autorzy ###

* Przemysław Kopiel
* Artur Dettlaff

### Link ###

* https://www.zacniewski.pl/vision/
